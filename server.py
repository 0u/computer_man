from flask import Flask, redirect
app = Flask(__name__)

@app.route('/')
def homepage():
    return redirect('https://youtu.be/jeg_TJvkSjg', code=302)

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8080)
